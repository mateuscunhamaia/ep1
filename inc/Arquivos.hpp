#ifndef Arquivos_hpp
#define Arquivos_hpp

#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>
#include <fstream>

#include "Cliente.hpp"
#include "Produto.hpp"
#include "Cadastro.hpp"

using namespace std;

void salvar(vector<Cliente*> &clientes);
void salvar(vector<Produto*> &estoque);

void carregar(vector<Cliente*> &clientes);
void carregar(vector<Produto*> &estoque);


#endif
