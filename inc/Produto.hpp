#ifndef Produto.hpp
#define Produto.hpp
#include <iostream>
#include <string>

using namespace std;

Class Produto{
    private:
    
    string Categoria, string Nome;
    int Quantidade;
    double Valor;
    
    public:

    Produto(string Categoria, string Nome, int Quantidade, double Valor)
    void set_Categoria(string Categoria);
    void set_Nome(string Nome);
    void set_Quantidade(int Quantidade);
    void set_Valor(double Valor);
    
    string get_Categoria();
    string get_Nome();
    int get_Quantidade();
    double get_Valor();
};  
#endif

