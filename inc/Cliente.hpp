#ifndef Cliente.hpp
#define Cliente.hpp
#include <string>
#include <iostream>
using namespace std

class Cliente
{
private:
    string Nome, string Email, string Sexo, string CPF, string Telefone;
    bool Socio;
    
public:
    
    Cliente();
    Cliente(string Nome, string Email, string Sexo, string CPF, string Telefone, bool Socio)
    void set_Nome(string Nome);
    void set_Email(string Email);
    void set_Sexo(string Sexo);
    void set_CPF(string CPF);
    void set_Telefone(string Telefone);
    void set_Socio(bool Socio);
    string get_Nome();
    string get_Email();
    string get_Sexo();
    string get_CPF();
    string get_Telefone();
    bool get_Socio();
    
};
#endif