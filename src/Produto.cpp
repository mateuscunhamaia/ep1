#include "Produto.hpp"
#include <iostream>
#include <string>

Produto::Produto(string Categoria, string Nome, int Quantidade, double Valor){
    this-> set_Categoria(Categoria);
    this-> set_Nome(Nome);
    this-> set_Quantidade(Quantidade);
    this-> set_Valor(Valor);
}
void Produto::set_Categoria(string Categoria){
    this-> Categoria = Categoria;
}
void Produto::set_Nome(string Nome){
    this-> Nome = Nome;
}
void Produto::set_Quantidade(int Quantidade){
    this-> Quantidade = Quantidade;
}
void Produto::set_Valor(double Valor){
    this-> Valor = Valor;
}
string Produto::get_Categoria(){
    return this-> Categoria;
}
string Produto::get_Nome(){
    return this-> Nome;
}
int Produto::get_Quantidade(){
    return this-> Quantidade;
}
double Produto::get_Valor(){
    return this-> Valor;
}