#include"Cliente.hpp"
#include<string>
#include<iostream>
using namespace std;

Cliente::Cliente(string Nome, string Email, string Sexo, string CPF, string Telefone, bool Socio){
    this->set_Nome(Nome);
    this->set_Email(Email);
    this->set_Sexo(Sexo);
    this->set_CPF(CPF);
    this->set_Telefone(Telefone);
    this->set_Socio(Socio);
}
Cliente::Cliente(){
    Nome = "n/a";
    Email = "n/a";
    Sexo = "n/a";
    CPF = 0;
    Telefone = 0;
    Socio = false;
}


void Cliente::set_Nome(string Nome){
    this->Nome = Nome;
}
void Cliente::set_Email(string Email){
    this->Email = Email;
}
void Cliente::set_Sexo(string Sexo){
    this->Sexo = Sexo;
}
void Cliente::set_CPF(string CPF){
    this->CPF = CPF;
}
void Cliente::set_Telefone(string Telefone){
    this->Telefone = Telefone;
}
void Cliente::set_Socio(bool Socio){
    this->Socio = Socio;
}
string Cliente::get_Nome(){
    return this-> Nome;
}
string Cliente::get_Email(){
    return this-> Email;
}
string Cliente::get_CPF(){
    return this-> CPF;
}
string Cliente::get_Telefone(){
    return this-> Telefone;
}
bool Cliente::get_Socio(){
    return this-> Socio;
}
